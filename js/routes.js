import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'

//User pages
import HomeUser from './user/views/Home';

//Administrator pages
import HomeAdmin from './admin/views/Home';

import Products from './admin/containers/Products';
import ProductsList from './admin/components/ProductsList';
import ProductAdd from './admin/components/ProductAdd';

import Orders from './admin/containers/Orders';
import OrdersList from './admin/components/OrdersList';

import Users from './admin/containers/Users';
import UsersList from './admin/components/UsersList';

export default () => (
    <Router>
        <div>
            <Route exact path="/" component={HomeUser} />
            <Route path="/admin" component={HomeAdmin} />

            <Route path="/admin/products" component={Products} />
            <Route path="/admin/products/list" component={ProductsList} />
            <Route path="/admin/products/add" component={ProductAdd} />

            <Route path="/admin/orders" component={Orders} />
            <Route path="/admin/orders/list" component={OrdersList} />

            <Route path="/admin/users" component={Users} />
            <Route path="/admin/users/list" component={UsersList} />

        </div>
    </Router>
);