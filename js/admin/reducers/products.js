﻿import {
    GET_PRODUCTS_LIST_REQUEST,
    GET_PRODUCTS_LIST_SUCCESS,
    GET_PRODUCTS_LIST_FAILURE
} from '../constants/Products'

const initialState = {
    products: "init products",
    user: "People"
}

export default function products(state = initialState, action) {

    switch (action.type) {

        case GET_PRODUCTS_LIST_REQUEST:
            console.log("GET_PRODUCTS_LIST_REQUEST");
            console.log(action.payload);
            return {
                products: action.payload
            }

        case GET_PRODUCTS_LIST_SUCCESS:
            console.log("GET_PRODUCTS_LIST_SUCCESS");
            console.log(action.payload);
            return {
                products: action.payload
            }

        case GET_PRODUCTS_LIST_FAILURE:
            console.log("GET_PRODUCTS_LIST_FAILURE");
            console.log(action.payload);
            return {
                products: action.payload
            }

        default:
            return state;
    }

}