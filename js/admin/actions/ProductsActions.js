import {
    GET_PRODUCTS_LIST_REQUEST,
    GET_PRODUCTS_LIST_SUCCESS,
    GET_PRODUCTS_LIST_FAILURE
} from '../constants/Products'

import {
    GET_PRODUCTS_LIST_URL
} from '../constants/CommonUrl'

import axios from "axios";

export function getProductsList() {

    return (dispatch) => {
        dispatch({
            type: GET_PRODUCTS_LIST_REQUEST
        })

        axios.get(GET_PRODUCTS_LIST_URL)
            .then((response) => {
                console.log(response);
                dispatch({
                    type: 'GET_PRODUCTS_LIST_SUCCESS',
                    payload: response.data
                });
            })
            .catch((err) => {
                dispatch({
                    type: 'GET_PRODUCTS_LIST_FAILURE',
                    payload: err
                })
            })
    }

}