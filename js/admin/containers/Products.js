import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as productsActions from '../actions/ProductsActions'

class Products extends React.Component {

    componentDidMount() {
        console.log(this.props);
        this.props.productsActions.getProductsList();
    }

    render() {
        
        return (
            <div className={this.props.products}>
                {this.props.children}
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        products: state.products.products,
        user: state.products.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        productsActions: bindActionCreators(productsActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products)