import React from 'react';

export default class ContainerContent extends React.Component {
    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}