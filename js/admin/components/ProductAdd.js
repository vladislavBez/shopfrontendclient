﻿import React from 'react';

export default class Products extends React.Component {
    render() {
        return (
            <div className="container-fluid">

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <div className="header_gray">Создать товар</div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <div className="toolbar">
                            <button className="button-toolbar">Сохранить</button>
                            <button className="button-toolbar">Сохранить и создать</button>
                            <button className="button-toolbar">Закрыть и сохранить</button>
                            <button className="button-toolbar">Закрыть не сохраняя</button>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        
                    </div>
                </div>

            </div>
        );
    }
}