﻿import React from 'react';
import { Link } from 'react-router-dom'

export default class ProductsList extends React.Component {
    render() {
        return (
            <div className="container-fluid">

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <div className="header_gray">Все товары</div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <div className="toolbar">
                            <Link to='/admin/products/add'>
                                <button className="button-toolbar">Создать</button>
                            </Link>
                            <Link to='/admin/products/edit'>
                                <button className="button-toolbar">Редактировать</button>
                            </Link>
                            <button className="button-toolbar">Удалить</button>
                            <Link to='/admin'>
                                <button className="button-toolbar button-toolbar-right">Закрыть</button>
                            </Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <table className="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Изображение</th>
                                    <th>Цена</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>1</th>
                                    <th>
                                        <div className="cart_img">
                                            <img src="../images/products/Harley-Davidson Dyna Wide Glide.jpg" />
                                        </div>
                                    </th>
                                    <th>500 000 р</th>
                                    <th><span className="light-gray">Убрать из корзины</span></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        );
    }
}