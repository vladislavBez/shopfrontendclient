﻿import React from 'react';
import { Link } from 'react-router-dom'

export default class TopMenu extends React.Component {
    render() {
        return (
            <div className="container-fluid admin_top_section">
                <div className="row">
                    <div className="top_menu">
                        <ul>
                            <li><Link to='/admin/products/list'>Товары</Link></li>
                            <li><Link to='/admin/orders/list'>Заказы</Link></li>
                            <li><Link to='/admin/users/list'>Пользователи</Link></li>
                        </ul>
                    </div>
                </div>
            </div>
            
        );
    }
}