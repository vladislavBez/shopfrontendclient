import React from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import ContainerContent from '../components/ContainerContent';
import CartView from '../components/CartView';

export default class Home extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <CartView />
                <Footer />
            </div>
        );
    }
}