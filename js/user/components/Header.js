import React from 'react';
import TopMenu from './TopMenu';
import HeaderContent from './HeaderContent';

export default class Header extends React.Component {
    render() {
        return (
            <div className="container-fluid top_section">
                <div className="row">
                    <TopMenu />
                </div>
                <div className="row">
                    <HeaderContent />
                </div>
            </div>
        );
    }
}