﻿import React from 'react';
import Product from './Product';
import ProductFilters from './ProductFilters';

export default class ProductsList extends React.Component {
    render() {
        return (
            <div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <ProductFilters />
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4 col-lg-4">
                        <Product />
                    </div>
                </div>

            </div>
        );
    }
}