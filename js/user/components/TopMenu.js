﻿import React from 'react';
import { Link } from 'react-router-dom'

export default class TopMenu extends React.Component {
    render() {
        return (
            <div className="pull-right top_menu">
                <ul>
                    <li><Link to='/login'>Вход</Link></li>
                    <li><Link to='/registration'>Регистрация</Link></li>
                </ul>
            </div>
        );
    }
}