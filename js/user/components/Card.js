﻿import React from 'react';
import { Link } from 'react-router-dom'

export default class Card extends React.Component {
    render() {
        return (
            <div>
                <div className="row">

                    <div className="col-md-6 col-lg-6">
                        <div className="card_img">
                            <img src="Harley-Davidson Dyna Wide Glide.jpg" />
                        </div>  
                    </div>

                    <div className="col-md-6 col-lg-6">
                        <div className="row">
                            <div className="col-md-12 col-lg-12">
                                <h1>Harley-Davidson Dyna Wide Glide</h1>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 col-lg-12">
                                <h3>1 150 000 р.</h3>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 col-lg-12">
                                <button className="button button-red">В корзину</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}