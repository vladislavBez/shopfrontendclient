﻿import React from 'react';

export default class Cart extends React.Component {
    render() {
        return (
            <div className="header_cart">
                <div className="cart_text cart_header">Корзина</div>
                <div className="cart_text cart_count">Товаров: 5</div>
                <div className="cart_text cart_price">Сумма: 5 000 000 р</div>
                <img src="cart.png"/>
            </div>
        );
    }
}