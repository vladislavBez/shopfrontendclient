﻿import React from 'react';

export default class CartView extends React.Component {
	render() {
		return (
            <div className="container-fluid cart_section">

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <h1>Корзина</h1>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <table className="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Изображение</th>
                                    <th>Цена</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>1</th>
                                    <th>
                                        <div className="cart_img">
                                            <img src="Harley-Davidson Dyna Wide Glide.jpg" />
                                        </div>
                                    </th>
                                    <th>500 000 р</th>
                                    <th><span className="light-gray">Убрать из корзины</span></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <h3 className="pull-right">Сумма: 500 000 р</h3>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12 col-lg-12">
                        <button className="button button-red pull-right">Оформить заказ</button>
                    </div>
                </div>

            </div>
        );
	}
}