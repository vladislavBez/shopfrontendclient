import React from 'react';
import Card from './Card';

export default class Content extends React.Component {
    render() {
        return (
            <div className="content_block">
                <Card />
                {this.props.children}
            </div>
        );
    }
}