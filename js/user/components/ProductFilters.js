﻿import React from 'react';
import { Link } from 'react-router-dom'

export default class ProductFilters extends React.Component {
    render() {
        return (
            <div className="filters_block">
                <div className="white filter_line">
                    Сортировать по цене:
                </div>
                <div className="filter_line">
                    <select>
                        <option selected value="0">Сначало подешевле</option>
                        <option value="1">Сначало подороже</option>
                    </select>
                </div>
            </div>
        );
    }
}