﻿import React from 'react';
import { Link } from 'react-router-dom'

export default class Product extends React.Component {
    render() {
        return (
            <div className="product_block">
                <Link to='/' className="gray">

                    <div className="product_img">
                        <div className="product_background">
                        </div>
                        <div className="product_button_container">
                            <div className="white product_more_info">
                                Клик - узнать больше...
                                    </div>
                            <button className="button button-red">В корзину</button>
                        </div>
                        <img src="../images/products/Harley-Davidson Dyna Wide Glide.jpg" />
                    </div>

                    <div className="product_header_container">
                        <div className="product_header product_name">
                            Harley-Davidson Dyna Wide Glide
                        </div>
                        <div className="product_header product_price">
                            1 150 000 р.
                        </div>
                    </div>

                </Link>
            </div>
        );
    }
}