﻿import React from 'react';
import { Link } from 'react-router-dom'

export default class MainMenu extends React.Component {
    render() {
        return (
            <div className="main_menu">
                <ul>
                    <li><Link to='/' className="">Главная</Link></li>
                    <li><Link to='/catalog' className="">Каталог</Link></li>
                    <li><Link to='/about' className="">О компании</Link></li>
                    <li><Link to='/contacts' className="">Контакты</Link></li>
                </ul>
            </div>
        );
    }
}