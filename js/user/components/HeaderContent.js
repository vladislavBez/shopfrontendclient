import React from 'react';
import Cart from './Cart';

export default class HeaderContent extends React.Component {
    render() {
        return (
            <div className="header_content">
                <div className="header_img">
                    <img src="header.png"/>
                </div>
                <Cart />
            </div>
        );
    }
}