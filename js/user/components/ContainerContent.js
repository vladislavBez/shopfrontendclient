import React from 'react';
import MainMenu from './MainMenu';
import Content from './Content';

export default class ContainerContent extends React.Component {
    render() {
        return (
            <div className="container-fluid content_section">
                <div className="row">
                    <div className="col-md-3 col-lg-3">
                        <MainMenu />
                    </div>
                    <div className="col-md-9 col-lg-9">
                        <Content />
                    </div>
                </div>
            </div>
        );
    }
}